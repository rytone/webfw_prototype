import "dart:async";
import "dart:io";
import "package:webfw_prototype/middleware.dart";
import "package:webfw_prototype/router/std_mw.dart";

Future<Null> serveHome(Context<HttpRequest> ctx) async {
  await ctx.request.response.write("test data");
}

void main() {
  final router = new Router()..get("/", serveHome);
  final mwController = new Controller<HttpRequest>()..use(router.route);

  HttpServer.bind("localhost", 80).then((server) {
    server.asyncMap(mwController.execute).listen((req) => req.response.close());
  });
}
