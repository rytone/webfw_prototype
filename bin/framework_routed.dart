import "dart:async";
import "package:webfw_prototype/middleware.dart";
import "package:webfw_prototype/framework.dart";

Future<Null> serveHome(Context<Request> ctx) async {
  await ctx.request.r.response.write("test data");
}

Future<Null> main() async {
  final app = new App.debug();
  app.routes.get("/", serveHome);
  await app.bindAndRun("localhost", 80);
}
