import "dart:async";
import "package:webfw_prototype/middleware.dart";
import "package:webfw_prototype/framework.dart";

Future<Null> serve(Context<Request> ctx) async {
  await ctx.request.r.response.write("test data");
  await ctx.next();
}

Future<Null> main() async {
  await new App.debug()
    ..use(serve)
    ..bindAndRun("localhost", 80);
}
