import "dart:async";
import "dart:io";
import "package:webfw_prototype/router/std.dart";

Future<Null> serveHome(HttpRequest req) async {
  await req.response.write("test data");
}

void main() {
  final router = new Router()..get("/", serveHome);

  HttpServer.bind("localhost", 80).then((server) {
    server.listen((req) async {
      await router.route(req);
      await req.response.close();
    });
  });
}
