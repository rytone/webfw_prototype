import "dart:async";
import "dart:io";
import "package:webfw_prototype/middleware.dart";

Future<Null> serve(Context<HttpRequest> ctx) async {
  await ctx.request.response.write("test data");
  await ctx.next();
}

void main() {
  final mwController = new Controller<HttpRequest>()..use(serve);

  HttpServer.bind("localhost", 80).then((server) {
    server.asyncMap(mwController.execute).listen((req) => req.response.close());
  });
}
