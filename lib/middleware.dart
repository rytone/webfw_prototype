import "dart:async";
import "dart:collection";

class Context<T> {
  final ListQueue<Handler> _nextMiddleware;

  final HashMap<String, dynamic> context;
  final T request;

  Context(this._nextMiddleware, this.request)
      : context = new HashMap<String, dynamic>();

  Future<Null> next() async {
    if (_nextMiddleware.isNotEmpty) {
      await _nextMiddleware.removeFirst()(this);
    }
  }

  void abort() => _nextMiddleware.clear();
}

typedef Future<Null> Handler<T>(Context<T> ctx);

class Controller<T> {
  final ListQueue<Handler<T>> _middleware;
  Controller() : _middleware = new ListQueue();

  void use(Handler<T> handler) => _middleware.add(handler);

  Future<T> execute(T req) async {
    final middleware = new ListQueue<Handler<T>>.from(_middleware);

    if (middleware.isNotEmpty) {
      await middleware.removeFirst()(new Context<T>(middleware, req));
    }

    return req;
  }
}
