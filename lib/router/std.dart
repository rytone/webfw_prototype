import "dart:async";
import "dart:io";
import "package:webfw_prototype/router/proto.dart" as proto;

typedef Future<Null> Handler(HttpRequest req);

class Router extends proto.Router<Handler, HttpRequest> {
  String requestPath(HttpRequest req) => req.uri.toString();
  String requestMethod(HttpRequest req) => req.method;
  void setStatus(HttpRequest req, int status) =>
      req.response.statusCode = status;
  Future<Null> dispatchHandler(HttpRequest req, Handler h) => h(req);
}
