import "dart:async";
import "dart:io";
import "package:webfw_prototype/router/proto.dart" as proto;
import "package:webfw_prototype/middleware.dart" as mw;

class Router
    extends proto.Router<mw.Handler<HttpRequest>, mw.Context<HttpRequest>> {
  String requestPath(mw.Context<HttpRequest> ctx) => ctx.request.uri.toString();
  String requestMethod(mw.Context<HttpRequest> ctx) => ctx.request.method;
  void setStatus(mw.Context<HttpRequest> ctx, int status) =>
      ctx.request.response.statusCode = status;
  Future<Null> dispatchHandler(
      mw.Context<HttpRequest> ctx, mw.Handler<HttpRequest> h) async {
    await h(ctx);
    await ctx.next();
  }
}
