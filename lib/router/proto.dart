import "dart:async";
import "dart:collection";

class _RouterEntry<T> {
  final String method;
  final T handler;

  _RouterEntry(this.method, this.handler);
}

class RouterTree<T> {
  final HashMap<String, _RouterEntry<T>> _entries;
  RouterTree() : _entries = new HashMap();

  void add(String method, String path, T handler) =>
      _entries[path] = new _RouterEntry(method, handler);
  _RouterEntry lookup(String method, String path) => _entries[path];
}

abstract class Router<H, R> {
  final RouterTree<H> _tree;
  Router() : _tree = new RouterTree();

  void get(String path, H handler) => _tree.add("GET", path, handler);
  void post(String path, H handler) => _tree.add("POST", path, handler);
  void patch(String path, H handler) => _tree.add("PATCH", path, handler);
  void put(String path, H handler) => _tree.add("PUT", path, handler);
  void delete(String path, H handler) => _tree.add("DELETE", path, handler);
  void head(String path, H handler) => _tree.add("HEAD", path, handler);
  void options(String path, H handler) => _tree.add("OPTIONS", path, handler);

  Future<Null> route(R req) async {
    final path = requestPath(req);
    final method = requestMethod(req);

    final entry = _tree.lookup(method, path);
    if (entry != null) {
      if (entry.method == method) {
        await dispatchHandler(req, entry.handler);
      } else {
        setStatus(req, 405);
      }
    } else {
      setStatus(req, 404);
    }
  }

  String requestPath(R req);
  String requestMethod(R req);
  void setStatus(R req, int status);
  Future<Null> dispatchHandler(R req, H handler);
}
