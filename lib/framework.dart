import "dart:async";
import "dart:io";
import "package:webfw_prototype/middleware.dart" as mw;
import "package:webfw_prototype/router/proto.dart" as rproto;

// TODO make this thing useful
class Request {
  final HttpRequest r;
  Request(this.r);
}

Future<Null> recover(mw.Context<Request> ctx) async {
  try {
    await ctx.next();
  } catch (e, s) {
    print(
        "Recovered from an unhandled ${e.toString()}\nStack trace:\n${s.toString()}");
    ctx.request.r.response.statusCode = 500;
    ctx.abort();
  }
}

// TODO make this thing not suck
Future<Null> logging(mw.Context<Request> ctx) async {
  final sw = new Stopwatch()..start();
  await ctx.next();
  print(
      "${ctx.request.r.response.statusCode.toString()} | ${ctx.request.r.method.toString()} | ${sw.elapsedMicroseconds.toString()}us | ${ctx.request.r.uri.toString()}");
}

class Router extends rproto.Router<mw.Handler<Request>, mw.Context<Request>> {
  String requestPath(mw.Context<Request> ctx) => ctx.request.r.uri.toString();
  String requestMethod(mw.Context<Request> ctx) => ctx.request.r.method;
  void setStatus(mw.Context<Request> ctx, int status) =>
      ctx.request.r.response.statusCode = status;
  Future<Null> dispatchHandler(
      mw.Context<Request> ctx, mw.Handler<Request> h) async {
    await h(ctx);
    await ctx.next();
  }
}

// TODO framework routing
class App {
  final mw.Controller<Request> _mwController;
  final Router routes;

  App()
      : _mwController = new mw.Controller(),
        routes = new Router() {
    _mwController.use(routes.route);
  }
  App.debug()
      : _mwController = new mw.Controller(),
        routes = new Router() {
    _mwController..use(logging)..use(recover)..use(routes.route);
  }

  void use(mw.Handler<Request> m) => _mwController.use(m);

  Future<Request> process(HttpRequest r) {
    final req = new Request(r);
    return _mwController.execute(req);
  }

  Future<Null> run(Stream<HttpRequest> server) => server
      .asyncMap(process)
      .listen((req) => req.r.response.close())
      .asFuture();

  Future<Null> bindAndRun(address, int port,
          {int backlog: 0, bool v6Only: false, bool shared: false}) =>
      HttpServer
          .bind(address, port, backlog: backlog, v6Only: v6Only, shared: shared)
          .then(run);
}
