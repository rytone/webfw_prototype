# webfw_prototype

this framework is designed to be as minimal and modular as possible, only
requiring the user to use the components they want. `framework.dart`,
`middleware.dart`, and `router/` will all become their own packages when
the time comes.

## examples

- `std_mw.dart` - just the middleware framework processing a `HttpRequest`
- `std_routed.dart` - just the router routing a `HttpRequest`, NO middleware
- `std_mw_routed.dart` - router + middleware framework with a `HttpRequest`
- `framework.dart` - full framework (uses `Request` instead of `HttpRequest`)
- `framework_routed.dart` - full framework + routing